package mono;
public class MonoPackageManager_Resources {
	public static String[] Assemblies = new String[]{
		/* We need to ensure that "wanted_android.dll" comes first in this list. */
		"wanted_android.dll",
		"MonoGame.Framework.dll",
		"wanted_library.dll",
	};
	public static String[] Dependencies = new String[]{
	};
	public static String ApiPackageName = "Mono.Android.Platform.ApiLevel_28";
}
