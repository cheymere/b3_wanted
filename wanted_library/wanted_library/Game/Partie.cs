﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace wanted_library.Game
{
    public class Partie
    {
        private Niveau Niv { get; set; }
        private Chrono chrono { get; set; }

        private Boolean fin = false;

        private Microsoft.Xna.Framework.Game game { get; set; }

        public Partie(Microsoft.Xna.Framework.Game game)
        {
            this.game = game;
            Niv = new Niveau(game);
            chrono = new Chrono();
        }

        private void reset()
        {
            fin = false;
            chrono = new Chrono();
            Niv = new Niveau(game);
        }


        public void niveauSup()
        {
            Niv.passerNiveau(game);
        }

        public void Draw(SpriteFont font, GameTime gameTime, SpriteBatch sprite)
        {
            if (fin)
            {
                Niv.reset();
                sprite.DrawString(font, "GAME OVER", new Vector2(Taille.Height/2 - 130, Taille.Width/2 - 150), Color.YellowGreen);
                sprite.DrawString(font, "Level : "+ Niv.Level, new Vector2(Taille.Height / 2 - 90, Taille.Width / 2 - 100), Color.YellowGreen);
                sprite.DrawString(font, "Touchez l'ecran", new Vector2(Taille.Height / 2 - 140, Taille.Width / 2 - 50), Color.YellowGreen);

            }
            else
            {
               chrono.Draw(font, sprite);
            }
            Niv.Draw(gameTime, sprite, font);
        }

        public void selection(TouchCollection touches)
        {
            if (fin)
            {
                foreach (var touche in touches)
                {
                    if (touche.State == TouchLocationState.Pressed)
                    {
                        reset();
                    }
                }
            }
            else
            {
                Entitee e = null;
                foreach (var touche in touches)
                {
                    if (touche.State == TouchLocationState.Pressed)
                    {
                        foreach (Entitee entitee in Niv.readOnlyBonus)
                        {
                            if (entitee._Texture.Bounds.Contains(touche.Position.X - entitee.Posx, touche.Position.Y - entitee.Posy))
                            {
                                e = entitee;
                                break;
                            }
                        }
                        if (e != null)
                        {
                            //passer niveau
                            chrono.bonusTime();
                            Niv.passerNiveau(game);
                            break;
                        }
                        else
                        {
                            if (chrono.malusTime())
                            {
                                fin = true;
                            }
                        }
                    }
                }
            }           
        }



        public void passerSeconde()
        {
            if (chrono.passerSeconde())
            {
                fin = true;
            }
        }

    }
}
