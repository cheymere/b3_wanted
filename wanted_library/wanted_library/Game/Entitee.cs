﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace wanted_library.Game
{
    public class Entitee : DrawableGameComponent
    {  

        //textures pour les images
    
        public Texture2D _Texture { get; set; }
        public Persos pers { get; private set; }
        public int Posx { get; set; }
        public int Posy { get; set; }

        //Vector2 EyePosition = new Vector2(30, 40);

        public Entitee(Microsoft.Xna.Framework.Game game, Persos pers) : base(game)
        {
            
            this.pers = pers;
            Random random = new Random();

            Posx = random.Next(10, Taille.Height - 100);
            Posy = random.Next(10, Taille.Width - 100);

            LoadContent(); //appelle la texture
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();
            
            switch (pers)
            {
                case Persos.GOBLIN:
                    _Texture = Game.Content.Load<Texture2D>("Asset/gobelin");
                    break;
                case Persos.EYE:
                    _Texture = Game.Content.Load<Texture2D>("Asset/eye");
                    break;
                case Persos.MUSHROOM:
                    _Texture = Game.Content.Load<Texture2D>("Asset/mushroom");
                    break;
                case Persos.SKELETTON:
                    _Texture = Game.Content.Load<Texture2D>("Asset/skeletton");
                    break;
            }

            //_textureEye = Game.Content.Load<Texture2D>("Assets/eye");
            //_textureMushroom = Game.Content.Load<Texture2D>("Assets/mushroom");
            //_textureSkeletton = Game.Content.Load<Texture2D>("Assets/skeletton");
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        
        public void Draw(GameTime gameTime, SpriteBatch sprite)
        {
            
            sprite.Draw(_Texture, new Vector2(Posx, Posy), Color.AntiqueWhite);
            base.Draw(gameTime);
        }

        public void DrawRecherchee(GameTime gameTime, SpriteBatch sprite)
        {  
            sprite.Draw(_Texture, new Vector2(Taille.Height/2, 150), Color.Wheat);
            base.Draw(gameTime);
        }
    }
}
