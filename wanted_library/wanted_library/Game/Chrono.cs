﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
namespace wanted_library.Game
{
    public class Chrono
    {
        private int time;



        public Chrono()
        {
            time = 30;
        }

        public bool passerSeconde()
        {
            time -= 1;
            if(time <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void bonusTime()
        {
            time += 5;
        }

        public bool malusTime()
        {
            time -= 10;
            if (time <= 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Draw(SpriteFont font, SpriteBatch sprite)
        {
            sprite.DrawString(font, time.ToString(), new Vector2(10, 50), Color.White);
        }

    }
}
