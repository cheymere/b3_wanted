﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;

namespace wanted_library.Game
{
    public class Niveau
    {
        public int Level { get;  private set; }
        private Persos choixPersos;
        private int nbEntitee = 10;

        private List<Entitee> lesEntiteesMalus = new List<Entitee>();
        public ReadOnlyCollection<Entitee> readOnlyMalus;

        private List<Entitee> entiteeBonus = new List<Entitee>();
        public ReadOnlyCollection<Entitee> readOnlyBonus;

        private List<Entitee> entiteeRecherchee = new List<Entitee>();
        public ReadOnlyCollection<Entitee> readOnlyRecherche;
    
        
        //ContentManager contentManager;

        public Niveau(Microsoft.Xna.Framework.Game game)
        {
            Level = 1;
            readOnlyMalus = new ReadOnlyCollection<Entitee>(lesEntiteesMalus);
            readOnlyBonus = new ReadOnlyCollection<Entitee>(entiteeBonus);
            readOnlyRecherche = new ReadOnlyCollection<Entitee>(entiteeRecherchee);

            Random random = new Random();
            Persos persoBonus = (Persos)random.Next(0, 4);
            entiteeBonus.Add(new Entitee(game, Persos.SKELETTON));

            for (int i = 0; i<nbEntitee; i++)
            {
                choixPersos = (Persos)random.Next(0, 3); //choix aleatoire des images
                lesEntiteesMalus.Add(new Entitee(game, choixPersos));
            }

            
            //entiteeRecherchee.Add(new Entitee(game, spriteBatch, Persos.SKELETTON));
        }

        public void reset()
        {
            lesEntiteesMalus = new List<Entitee>();
            entiteeBonus = new List<Entitee>();

            readOnlyMalus = new ReadOnlyCollection<Entitee>(lesEntiteesMalus);
            readOnlyBonus = new ReadOnlyCollection<Entitee>(entiteeBonus);
        }

        public void passerNiveau(Microsoft.Xna.Framework.Game game)
        {
            reset();
            Level += 1;
            Random random = new Random();

            Persos choixPersosBonus = (Persos)random.Next(0, 4);
            entiteeBonus.Add(new Entitee(game, choixPersos));

            for (int i = 0; i < nbEntitee + 5; i++)
            {
                
                choixPersos = (Persos)random.Next(0, 4); //choix aleatoire des images
                if(choixPersos == entiteeBonus[0].pers)
                {
                    while(choixPersos == entiteeBonus[0].pers)
                    {
                        choixPersos = (Persos)random.Next(0, 4); //choix aleatoire des images
                    }
                }
                lesEntiteesMalus.Add(new Entitee(game, choixPersos));
            }
        }

        internal void Draw(GameTime gameTime,SpriteBatch sprite, SpriteFont font)
        {
            sprite.DrawString(font, Level.ToString(), new Vector2(Taille.Height/2, 50), Color.YellowGreen);
            foreach (Entitee entitee in readOnlyMalus)
            {
                entitee.Draw(gameTime,sprite);
            }
            foreach(Entitee entitee in readOnlyBonus)
            {
                entitee.Draw(gameTime,sprite);
            }
            foreach (Entitee entitee in readOnlyRecherche)
            {
                entitee.DrawRecherchee(gameTime,sprite);
            }
        }
    }
}
