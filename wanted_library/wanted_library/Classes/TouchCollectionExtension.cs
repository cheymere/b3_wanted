﻿using Microsoft.Xna.Framework.Input.Touch;

namespace wanted_library.Classes
{
    public static class TouchCollectionExtension
    {
        public static bool AnyTouch(this TouchCollection touchState)
        {
            foreach (TouchLocation location in touchState)
            {
                if (location.State == TouchLocationState.Pressed || location.State == TouchLocationState.Moved)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
