﻿using System;
using System.Diagnostics;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using wanted_library.Game;

namespace wanted_library
{
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private SpriteFont font;

        private string tapToStart = "Tap to start";

        private Niveau Niv { get; set; }
        private Chrono chrono { get; set; }

        private Partie partie { get; set; }

        private TouchCollection touchState;

        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            _graphics.IsFullScreen = true;

            Taille.Height = _graphics.PreferredBackBufferHeight;
            Taille.Width = _graphics.PreferredBackBufferWidth;

            //b = TouchPanel.GetCapabilities().IsConnected; //savoir si le touch est bien présent
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            partie = new Partie(this);
            TouchPanel.EnabledGestures = GestureType.Tap;
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            
            font = Content.Load<SpriteFont>("Font/TextFont");

            // TODO: use this.Content to load your game content here
            //marioWanted = Content.Load<Texture2D>("wantedmario");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            HandleInput();

            // TODO: Add your update logic here
            base.Update(gameTime);
        }

        private void HandleInput()
        {
            partie.selection(TouchPanel.GetState());
           
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            Trace.WriteLine(DateTime.Now.ToString() + " - Start of " + "draw");

            // TODO: Add your drawing code here

            if(gameTime.TotalGameTime.Milliseconds % 10000 == 0)
            {
                partie.passerSeconde();
            }
           
            _spriteBatch.Begin();
            partie.Draw(font, gameTime, _spriteBatch);
            _spriteBatch.End();           

            base.Draw(gameTime);
        }
    }
}
